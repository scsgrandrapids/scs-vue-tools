module.exports = {
    ScsPlugin: require('./ScsPlugin').default,
    ScsValidation: require('./ScsValidation').default,
    ScsLaravel: require('./ScsLaravel'),
    ScsClear: require('./ScsClear')
};