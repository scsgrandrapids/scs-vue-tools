module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmony imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 108);
/******/ })
/************************************************************************/
/******/ ({

/***/ 108:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(5);


/***/ }),

/***/ 11:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

exports.default = function (tDefinition) {
    return function (aState, aGetters) {
        var lDefinition = tDefinition;
        var lValid = true;
        var lErrors = [];
        var lCount = 0;

        if (_.isString(lDefinition)) {
            var lValdations = aGetters[lDefinition + 'IsValid'];
            _.each(lValdations, function (tValidation) {
                if (!tValidation.valid) {
                    lValid = false;
                    lErrors.push(tValidation.message);
                    lCount++;
                }
            });
        } else if (_.isArray(lDefinition)) {
            _.each(lDefinition, function (tValidator) {
                var lValdations = aGetters[tValidator + 'IsValid'];
                _.each(lValdations, function (tValidation) {
                    if (!tValidation.valid) {
                        lValid = false;
                        lErrors.push(tValidation.message);
                        lCount++;
                    }
                });
            });
        } else {
            lValid = false;
            lCount++;
        }

        return {
            valid: lValid,
            errors: lErrors,
            count: lCount
        };
    };
};

/***/ }),

/***/ 12:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

exports.default = function (aGetter, aValidators) {
    return function (aState, aGetters) {
        var lGetter = aGetter;
        var lValidators = aValidators;
        return _.map(lValidators, function (tValidator, tKey) {
            var lMessage = Vue.validators[tKey](aGetters[lGetter], tValidator, aGetters);
            var lValid = _.trim(lMessage) == '';
            return {
                message: lMessage,
                valid: lValid
            };
        });
    };
};

/***/ }),

/***/ 13:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

exports.default = function (aDefinitions) {
    return _.mapValues(aDefinitions, function (tDefinition) {
        return Vue.makeValidationGroup(tDefinition);
    });
};

;

/***/ }),

/***/ 14:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

exports.default = function (aDefinitions) {
    var lKeys = _.keys(aDefinitions);

    var lValidators = _.chain(aDefinitions).mapValues(function (tDefinition, tKey) {
        return Vue.makeValidator(tKey, tDefinition);
    }).mapKeys(function (tDefinition, tKey) {
        return tKey + 'IsValid';
    }).value();

    return _.extend(lValidators, {
        'allValid': Vue.makeValidationGroup(lKeys)
    });
};

/***/ }),

/***/ 16:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

exports.default = function (aValue, aArguments, aGetters) {
    var lValue = _.trim(aValue);
    var lMessage = void 0;
    if (typeof aArguments.message == 'undefined') {
        lMessage = 'Please enter a valid email address';
    } else {
        lMessage = aArguments.message;
    }

    if (typeof lValue == 'undefined' || lValue == null || lValue == '') {
        return '';
    }

    var lSplitEmailAddresses = function lSplitEmailAddresses(emailAddresses, separator) {
        var quotedFragments = emailAddresses.split(/"/),
            quotedFragmentCount = quotedFragments.length,
            emailAddressArray = [],
            nextEmailAddress = '';

        for (var i = 0; i < quotedFragmentCount; i++) {
            if (i % 2 === 0) {
                var splitEmailAddressFragments = quotedFragments[i].split(separator),
                    splitEmailAddressFragmentCount = splitEmailAddressFragments.length;

                if (splitEmailAddressFragmentCount === 1) {
                    nextEmailAddress += splitEmailAddressFragments[0];
                } else {
                    emailAddressArray.push(nextEmailAddress + splitEmailAddressFragments[0]);

                    for (var j = 1; j < splitEmailAddressFragmentCount - 1; j++) {
                        emailAddressArray.push(splitEmailAddressFragments[j]);
                    }
                    nextEmailAddress = splitEmailAddressFragments[splitEmailAddressFragmentCount - 1];
                }
            } else {
                nextEmailAddress += '"' + quotedFragments[i];
                if (i < quotedFragmentCount - 1) {
                    nextEmailAddress += '"';
                }
            }
        }

        emailAddressArray.push(nextEmailAddress);
        return emailAddressArray;
    };

    // Email address regular expression
    // http://stackoverflow.com/questions/46155/validate-email-address-in-javascript
    var lEmailRegExp = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/;
    var lAllowMultiple = aArguments.multiple === true || aArguments.multiple === 'true';

    if (lAllowMultiple) {
        var separator = aArguments.separator || /[,;]/,
            addresses = lSplitEmailAddresses(value, separator);

        for (var i = 0; i < addresses.length; i++) {
            if (!lEmailRegExp.test(addresses[i])) {
                return lMessage;
            }
        }

        return lMessage;
    } else if (lEmailRegExp.test(value)) {
        return '';
    } else {
        return lMessage;
    }
};

;

/***/ }),

/***/ 17:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

exports.default = function (aValue, aArguments, aGetters) {
    var lValue = _.trim(aValue);
    var lMessage = void 0;
    if (typeof aArguments['message'] == 'undefined') {
        lMessage = 'Please enter a value';
    } else {
        lMessage = aArguments['message'];
    }

    if (typeof aArguments['compare'] == 'undefined') {
        console.warn('SCS Validation: required comparison argument is missing');
        return lMessage;
    } else if (typeof aGetters.validators[aArguments['compare']] == 'undefined') {
        console.warn('SCS Validation: required comparison field does not exist is missing');
        return lMessage;
    } else if (aGetters[aArguments['compare']] == aValue) {
        return '';
    } else {
        return lMessage;
    }
};

;

/***/ }),

/***/ 18:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

exports.default = function (aValue, aArguments, aGetters) {
    var lValue = _.trim(aValue);
    var lMessage = void 0;
    if (typeof aArguments['message'] == 'undefined') {
        lMessage = 'Please enter a value';
    } else {
        lMessage = aArguments['message'];
    }

    if (_.isString(lValue)) {
        if (typeof lValue == 'undefined' || lValue == null || lValue == '') {
            return lMessage;
        } else {
            return '';
        }
    } else if (_.isArray(lValue) || _.isPlainObject(lValue)) {
        if (_.isEmpty(lValue)) {
            return lMessage;
        } else {
            return '';
        }
    } else {
        return lMessage;
    }
};

;

/***/ }),

/***/ 5:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
		value: true
});
exports.default = {
		install: function install(Vue, options) {
				Vue.validators = {
						required: __webpack_require__(18).default,
						email: __webpack_require__(16).default,
						identical: __webpack_require__(17).default
				};

				Vue.makeValidator = __webpack_require__(12).default;

				Vue.makeValidationGroup = __webpack_require__(11).default;

				Vue.mapValidators = __webpack_require__(14).default;

				Vue.mapValidationGroups = __webpack_require__(13).default;
		}
};

/***/ })

/******/ });