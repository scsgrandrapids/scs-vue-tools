module.exports = function(fs, _, path) {
    if(typeof(path) == 'undefined') {
        path = '';
    }

    //Delete Old JS files
    _.each(fs.readdirSync(path + 'public/js'), (file) => {
        if(
          _.startsWith(file, 'ScsApp')
          || _.startsWith(file, 'app')
        ) {
            fs.unlinkSync(path + 'public/js/' + file);
        }
    });

    //Delete Old CSS files
    _.each(fs.readdirSync(path + 'public/css'), (file) => {
        if(
          _.startsWith(file, 'ScsApp')
          || _.startsWith(file, 'app')
        ) {
            fs.unlinkSync(path + 'public/css/' + file);
        }
    });

    //Delete mix manifest files
    _.each(fs.readdirSync(path + 'public'), (file) => {
        if(_.startsWith(file, 'mix')) {
            fs.unlinkSync(path + 'public/' + file);
        }
    });
}