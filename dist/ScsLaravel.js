module.exports = function(mix, path) {
	if(typeof(path) == 'undefined') {
		path = './';
	}
	// update any vue tool assets
	mix.copyDirectory('node_modules/scs-vue-tools/Laravel', path);
};