const { mix } = require('laravel-mix');

mix.js('Vue/ScsPlugin.js', 'dist/ScsPlugin.js')
	.webpackConfig({
        output: {
        	libraryTarget: 'commonjs2',
        	library: 'dist/ScsPlugin.js'
        }
   });

mix.js('Vue/ScsValidation.js', 'dist/ScsValidation.js')
	.webpackConfig({
        output: {
        	libraryTarget: 'commonjs2',
        	library: 'dist/ScsValidation.js'
        }
   });

mix.combine([
    'node_modules/jquery/dist/jquery.js',
	'node_modules/bootstrap/dist/js/bootstrap.js',
	'node_modules/chartjs/chart.js',
	'node_modules/select2/dist/js/select2.full.js',
	'node_modules/jquery.inputmask/dist/inputmask/inputmask.js',
	'node_modules/jquery.inputmask/dist/inputmask/inputmask.date.extensions.js',
	'node_modules/jquery.inputmask/dist/inputmask/inputmask.extensions.js',
	'node_modules/jquery.inputmask/dist/inputmask/jquery.inputmask.js',
	'node_modules/moment/moment.js',
	'node_modules/bootstrap-daterangepicker/daterangepicker.js',
	'node_modules/bootstrap-datepicker/dist/js/bootstrap-datepicker.js',
	'node_modules/smalot-bootstrap-datetimepicker/js/bootstrap-datetimepicker.js',
	'node_modules/jquery-timepicker/jquery.timepicker.js',
	'node_modules/datatables.net/js/jquery.dataTables.js',
	'node_modules/datatables/media/js/jquery.dataTables.js',
	'node_modules/datatables.net-responsive/js/dataTables.responsive.js',
	'node_modules/jquery-slimscroll/jquery.slimscroll.js',
	'node_modules/trix/dist/trix.js',
	'node_modules/admin-lte/dist/js/adminlte.js',
	'JQuery/plugins/formvalidation/js/formValidation.js',
	'JQuery/plugins/formvalidation/js/framework/bootstrap.js',
], 'Laravel/public/js/jquery-dependencies.js');

mix.combine([
    'node_modules/bootstrap/dist/css/bootstrap.css',
	'node_modules/font-awesome/css/font-awesome.css',
	'node_modules/bootstrap-daterangepicker/daterangepicker.css',
	'node_modules/jquery-timepicker/jquery.timepicker.css',
	'node_modules/bootstrap-datepicker/dist/css/bootstrap-datepicker.css',
	'node_modules/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css',
	'node_modules/smalot-bootstrap-datetimepicker/css/bootstrap-datetimepicker.css',
	'node_modules/datatables/media/css/jquery.dataTables.css',
	'node_modules/datatables.net-responsive-dt/css/responsive.dataTables.css',
	'node_modules/select2/dist/css/select2.css',
	'node_modules/trix/dist/trix.css',
	'node_modules/admin-lte/dist/css/AdminLTE.css',
	'node_modules/admin-lte/dist/css/skins/skin-blue.css',
	'JQuery/plugins/formvalidation/css/formValidation.css',
	'node_modules/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css'
], 'Laravel/public/css/jquery-dependencies.css');

// glyphicons fonts
mix.copy('node_modules/bootstrap/fonts', 'Laravel/public/fonts');

// fontawesome fonts
mix.copy('node_modules/font-awesome/fonts', 'Laravel/public/fonts');