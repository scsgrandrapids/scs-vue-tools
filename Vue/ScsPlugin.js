import { mapGetters } from 'vuex';

export default {
	install(Vue, options) {
		Vue.aclCan = require('./Functions/AclCan').default(options);

		Vue.webRoute = require('./Functions/WebRoute').default(options);

		Vue.prototype.$aclCan = Vue.aclCan;

		Vue.prototype.$webRoute = Vue.webRoute;

		Vue.mixin({
			computed: {
		        ...mapGetters({
		            aclPermissions: 'aclPermissions',
		            currentUser: 'currentUser',
		            routes: 'routes',
		        }),
		    }
		})

		Vue.filter('date', require('./Filters/Date').default(options));
		Vue.filter('time', require('./Filters/Time').default(options));
		Vue.filter('datetime', require('./Filters/DateTime').default(options));

		Vue.component('page-loading', require('./Components/PageLoading.vue'));
		Vue.component('modal', require('./Components/Modal.vue'));

		Vue.component('vue-form', require('./Components/VueForm.vue'));
		Vue.component('validate-input', require('./Components/VueForm/ValidateInput.vue'));
		Vue.component('validate', require('./Components/VueForm/Validate.vue'));

		Vue.component('validation-group', require('./Components/ValidationGroup.vue'));
		Vue.component('validator', require('./Components/Validator.vue'));

		Vue.component('select2', require('./Components/Select2.vue'));
		Vue.component('button-group', require('./Components/ButtonGroup.vue'));
		Vue.component('number', require('./Components/Number.vue'));
		Vue.component('textbox', require('./Components/TextBox.vue'));
		Vue.component('password', require('./Components/Password.vue'));
		Vue.component('checkbox', require('./Components/CheckBox.vue'));
		Vue.component('delete', require('./Components/Delete.vue'));
		Vue.component('rich-text-editor', require('./Components/RichTextEditor.vue'));
		Vue.component('btn', require('./Components/Button.vue'));
		Vue.component('submit-button', require('./Components/SubmitButton.vue'));

		Vue.component('date', require('./Components/Date.vue'));
		Vue.component('timepicker', require('./Components/Time.vue'));
		Vue.component('datetime', require('./Components/DateTime.vue'));

		Vue.component('file-uploader', require('./Components/FileUploader.vue'));
		Vue.component('file-viewer', require('./Components/FileViewer.vue'));
	}
};