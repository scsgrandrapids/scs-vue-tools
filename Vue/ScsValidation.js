export default {
	install(Vue, options) {
		Vue.validators = {
			required: require('./Validators/Required').default,
			email: require('./Validators/Email').default,
			identical: require('./Validators/Identical').default,
		};

		Vue.makeValidator = require('./Functions/MakeValidator').default;

		Vue.makeValidationGroup = require('./Functions/MakeValidationGroup').default;

		Vue.mapValidators = require('./Functions/MapValidators').default;

		Vue.mapValidationGroups = require('./Functions/MapValidationGroups').default;
	}
};