export default function(aDefinitions) {
    let lKeys = _.keys(aDefinitions);

    let lValidators = _.chain(aDefinitions)
        .mapValues((tDefinition, tKey) => {
            return Vue.makeValidator(tKey, tDefinition);
        })
        .mapKeys((tDefinition, tKey) => {
            return tKey + 'IsValid';
        })
        .value();

    return _.extend(lValidators, 
        {
            'allValid': Vue.makeValidationGroup(lKeys)
        }
    );
}