export default function(aDefinitions) {
    return _.mapValues(aDefinitions, (tDefinition) => {
        return Vue.makeValidationGroup(tDefinition);
    });
};