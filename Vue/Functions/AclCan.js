export default function(options) {
	return function(action, group, object) {
        if (group == 'admin') {
            let user = _.clone(options.store.state.currentUser);
            return user.is_admin;
        }

        if ((typeof(object) != 'undefined') && (action != 'create')) {
            let user = _.clone(options.store.state.currentUser);
            if (group == 'users') {
                if (user.id == object.id) {
                    return true;
                }
            } else {
                if (user.id == object.user_id) {
                    return true;
                }
            }
        }
        if(typeof(options.store.state.aclPermissions[group]) != 'undefined') {
            if(options.store.state.aclPermissions[group][action] == true) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    };
};