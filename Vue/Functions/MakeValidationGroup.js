export default function(tDefinition) {
    return (aState, aGetters) => {
        let lDefinition = tDefinition;
        let lValid = true;
        let lErrors = [];
        let lCount = 0;

        if(_.isString(lDefinition)) {
            let lValdations = aGetters[lDefinition + 'IsValid'];
            _.each(lValdations, (tValidation) => {
                if(!tValidation.valid) {
                    lValid = false;
                    lErrors.push(tValidation.message);
                    lCount++;
                }
            });
        } else if(_.isArray(lDefinition)) {
            _.each(lDefinition, (tValidator) => {
                let lValdations = aGetters[tValidator + 'IsValid'];
                _.each(lValdations, (tValidation) => {
                    if(!tValidation.valid) {
                        lValid = false;
                        lErrors.push(tValidation.message);
                        lCount++;
                    }
                });    
            });
        } else {
            lValid = false;
            lCount++;
        }

        return {
            valid: lValid,
            errors: lErrors,
            count: lCount
        };
    }
}
