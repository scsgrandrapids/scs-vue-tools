export default function(aGetter, aValidators) {
    return (aState, aGetters) => {
        let lGetter = aGetter;
        let lValidators = aValidators;
        return _.map(lValidators, (tValidator, tKey) => {
            const lMessage = Vue.validators[tKey](aGetters[lGetter], tValidator, aGetters);
            const lValid = (_.trim(lMessage) == '');
            return {
                message: lMessage,
                valid: lValid,
            };
        });
    }
}