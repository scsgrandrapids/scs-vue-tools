export default function(options) {
	return function(name, parameters, queryVars) {
        let route = _.clone(options.store.state.routes[name]);
        let queryString = [];
        let keys = [];
        let uri = route.uri;

        if(typeof(route) == 'undefined') {
            console.warn('ERROR: route not found');
            console.warn('Route Name: ' + name);
            console.warn('');
            return '#';
        }
        
        if(_.isPlainObject(parameters)) {
            _.each(parameters, (value, key) => {
                if(_.includes(route.parameters, key)) {
                    uri = uri.replace('_' + key + '_', value);
                } else {
                    console.warn('ERROR: parameter not found');
                    console.warn('Route Name: ' + name);
                    console.warn('Parameter Name: ' + key);
                    console.warn('');
                }
            });
        } else if(_.isArray(parameters)) {
            _.each(route.parameters, (value, index) => {
                if(index < _.size(parameters)) {
                    uri = uri.replace('_' + value + '_', parameters[index]);    
                }
            });
        }

        if(_.isPlainObject(queryVars)) {
            queryString = _.map(queryVars, (value, key) => {
                return encodeURIComponent(key) + '=' + encodeURIComponent(value);
            });
        }

        if(queryString.length > 0) {
            uri = uri + '?' + queryString.join('&');
        }

        return uri;
    };
};