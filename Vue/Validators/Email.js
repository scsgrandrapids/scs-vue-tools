export default function(aValue, aArguments, aGetters) {
    const lValue = _.trim(aValue);
    let lMessage;
    if(typeof(aArguments.message) == 'undefined') {
        lMessage = 'Please enter a valid email address';
    } else {
        lMessage = aArguments.message;
    }

    if(
        typeof(lValue) == 'undefined'
        || lValue == null
        || lValue == ''
    ) {
        return '';
    }

    const lSplitEmailAddresses = function(emailAddresses, separator) {
        var quotedFragments     = emailAddresses.split(/"/),
            quotedFragmentCount = quotedFragments.length,
            emailAddressArray   = [],
            nextEmailAddress    = '';

        for (var i = 0; i < quotedFragmentCount; i++) {
            if (i % 2 === 0) {
                var splitEmailAddressFragments     = quotedFragments[i].split(separator),
                    splitEmailAddressFragmentCount = splitEmailAddressFragments.length;

                if (splitEmailAddressFragmentCount === 1) {
                    nextEmailAddress += splitEmailAddressFragments[0];
                } else {
                    emailAddressArray.push(nextEmailAddress + splitEmailAddressFragments[0]);

                    for (var j = 1; j < splitEmailAddressFragmentCount - 1; j++) {
                        emailAddressArray.push(splitEmailAddressFragments[j]);
                    }
                    nextEmailAddress = splitEmailAddressFragments[splitEmailAddressFragmentCount - 1];
                }
            } else {
                nextEmailAddress += '"' + quotedFragments[i];
                if (i < quotedFragmentCount - 1) {
                    nextEmailAddress += '"';
                }
            }
        }

        emailAddressArray.push(nextEmailAddress);
        return emailAddressArray;
    }

    // Email address regular expression
    // http://stackoverflow.com/questions/46155/validate-email-address-in-javascript
    const lEmailRegExp   = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/;
    const lAllowMultiple = aArguments.multiple === true || aArguments.multiple === 'true';

    if (lAllowMultiple) {
        var separator = aArguments.separator || /[,;]/,

            addresses = lSplitEmailAddresses(value, separator);

        for (var i = 0; i < addresses.length; i++) {
            if (!lEmailRegExp.test(addresses[i])) {
                return lMessage;
            }
        }

        return lMessage;
    } else if(lEmailRegExp.test(value)) {
        return '';
    } else {
        return lMessage;
    }
};


