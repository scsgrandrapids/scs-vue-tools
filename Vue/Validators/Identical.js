export default function(aValue, aArguments, aGetters) {
    const lValue = _.trim(aValue);
    let lMessage;
    if(typeof(aArguments['message']) == 'undefined') {
        lMessage = 'Please enter a value';
    } else {
        lMessage = aArguments['message'];
    }

    if(typeof(aArguments['compare']) == 'undefined') {
        console.warn('SCS Validation: required comparison argument is missing');
        return lMessage;
    } else if(typeof(aGetters.validators[aArguments['compare']]) == 'undefined') {
        console.warn('SCS Validation: required comparison field does not exist is missing');
        return lMessage;
    } else if(aGetters[aArguments['compare']] == aValue) {
        return '';
    } else {
        return lMessage;
    }
};