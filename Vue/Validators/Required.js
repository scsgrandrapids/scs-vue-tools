export default function(aValue, aArguments, aGetters) {
    const lValue = _.trim(aValue);
    let lMessage;
    if(typeof(aArguments['message']) == 'undefined') {
        lMessage = 'Please enter a value';
    } else {
        lMessage = aArguments['message'];
    }

    if(_.isString(lValue)) {
        if(
            typeof(lValue) == 'undefined'
            || lValue == null
            || lValue == ''
        ) {
            return lMessage;
        } else {
            return '';
        }
    } else if(_.isArray(lValue) || _.isPlainObject(lValue)) {
        if(_.isEmpty(lValue)) {
            return lMessage;
        } else {
            return '';
        }
    } else {
        return lMessage;
    }
};