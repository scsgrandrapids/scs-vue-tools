export default function(options) {
    return function (aDateTime, aDateTimeFormat) {
        if (!aDateTime) {
            return '';
        }

        let lDateTimeFormat;
        let lDatabaseDateTimeFormat = options.store.state.vueConfig.databaseDateTimeFormat;

        if(typeof(aDateTimeFormat) == 'undefined') {
        	lDateTimeFormat = options.store.state.vueConfig.dateTimeFormat;
        } else {
        	lDateTimeFormat = aDateTimeFormat;
        }

        return moment(aDateTime, lDatabaseDateTimeFormat)
        	.format(lDateTimeFormat);
    }
}