export default function(options) {
    return function (aDate, aDateFormat) {
        if (!aDate) {
            return '';
        }

        let lDateFormat;
        let lDatabaseDateFormat = options.store.state.vueConfig.databaseDateFormat;

        if(typeof(aDateFormat) == 'undefined') {
        	lDateFormat = options.store.state.vueConfig.dateFormat;
        } else {
        	lDateFormat = aDateFormat;
        }

        return moment(aDate, lDatabaseDateFormat)
        	.format(lDateFormat);
    }
}