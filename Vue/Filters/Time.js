export default function(options) {
    return function (aTime, aTimeFormat) {
        if (!aTime) {
            return '';
        }

        let lTimeFormat;
        let lDatabaseTimeFormat = options.store.state.vueConfig.databaseTimeFormat;

        if(typeof(aTimeFormat) == 'undefined') {
        	lTimeFormat = options.store.state.vueConfig.timeFormat;
        } else {
        	lTimeFormat = aTimeFormat;
        }

        return moment(aTime, lDatabaseTimeFormat)
        	.format(lTimeFormat);
    }
}