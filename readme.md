scs-vue-tools
==============

<h3>SCS Vue Tools for Vue2.x Laravel Interfaces</h3>

Step 1:

Add this repository to NPM in package.json:
```
"scs-vue-tools": "git+ssh://git@github.com/AgilityDataSystems/scs-vue-tools.git#4.X.X"
```

run
```
yarn install
```

Step 2:

Include the following in webpack.mix.js
```
let fs = require('fs');
let _ = require('lodash');

let scsLaravel = require('scs-vue-tools').ScsLaravel;
let scsClear = require('scs-vue-tools').ScsClear;

//Copies laravel specific files
scsLaravel(mix);

//Clears Old JS/CSS compiled files
scsClear(fs, _);
```

Step 3:

On the first install, we need to add the Vue URL routing code to your main website template:
```
@include('layouts.partials.vue')
```

Step 4:

We need to include the vue tools in resources/assets/js/app.js
```
import { ScsPlugin } from 'scs-vue-tools';

window.Vue.use(ScsPlugin, {store});
```

Step 5:

Now we just need to compile the assets to have access to vue tools.

Run:
```
yarn run dev
```