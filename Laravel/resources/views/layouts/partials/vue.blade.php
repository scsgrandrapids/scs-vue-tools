<?php

class ScsVueData {
    public static function getRoutes() {
        $routesData = Route::getRoutes();
        $routes = collect();

        foreach($routesData as $data) {
            $name;
            $uri;
            
            if(!is_null($data->getName())) {
                $name = $data->getName();
                $parameters = [];
                if(count($data->parameterNames()) > 0) {
                    $parametersData = $data->parameterNames();
                    foreach($parametersData as $parameter) {
                        $parameters[] = '_' . $parameter . '_';
                    }
                }
                $uri = route($data->getName(), $parameters);

                $routes->put($name, ['uri' => $uri, 'parameters' => $data->parameterNames()]);
            } else {
                $uri = '/' . $data->uri();
                $uri = str_replace('{', '_', $uri);
                $uri = str_replace('}', '_', $uri);

                $name = str_replace('/', '.', $data->uri());
            }

            $routes->put($name, ['uri' => $uri, 'parameters' => $data->parameterNames()]);
        }
        $routes->put('!previous', ['uri' => url()->previous(), 'parameters' => []]);

        return $routes;
    }

    public static function getPermissions() {
        $groups = [];
        if (\Auth::check()) {
            $groups = \App\Models\SecurityGroup::leftJoin('security_permissions', 'security_permissions.security_group_id', '=', 'security_groups.id')
                ->where('security_permissions.security_role_id', '=', auth()->user()->role_id)
                ->orderBy('security_groups.id', 'asc')
                ->select(
                    'security_groups.short_name', 
                    'security_permissions.access',
                    'security_permissions.view',
                    'security_permissions.create',
                    'security_permissions.modify',
                    'security_permissions.delete'
                )
                ->get();
        }

        $permissions = [];
        foreach($groups as $group) {
            $permissions[$group->short_name] = $group->toArray();
        }

        return collect($permissions);
    }
}

?>

<script>
window.Laravel = <?php echo json_encode([
    'csrfToken' => csrf_token(),
    'currentUser' => auth()->user(),
    'url' => config('app.url'),
    'vueConfig' => config('vue'),
    'siteConfig' => config('site'),
    'routes' => ScsVueData::getRoutes(),
    'permissions' => ScsVueData::getPermissions()
]); ?>

</script>
