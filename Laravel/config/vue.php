<?php

return [
	/* this contains useful defaults that are used by the vue tools framework */
	'databaseDateFormat' => env('DB_DATE_FORMAT', 'YYYY-MM-DD'),
	'databaseTimeFormat' => env('DB_TIME_FORMAT', 'HH:mm:ss'),
	'databaseDateTimeFormat' => env('DB_DATETIME_FORMAT', 'YYYY-MM-DD HH:mm:ss'),

	'dateFormat' => env('DATE_FORMAT', 'M/D/YY'),
	'timeFormat' => env('TIME_FORMAT', 'h:mma'),
	'dateTimeFormat' => env('DATETIME_FORMAT', 'M/D/YY h:mma'),

	'datePickerFormat' => env('DATEPICKER_FORMAT', 'mm/dd/yy'),
	'timePickerFormat' => env('TIMEPICKER_FORMAT', 'h:mma'),
	'dateTimePickerFormat' => env('DATETIMEPICKER_FORMAT', 'mm/dd/yy h:mma'),

	'defaultDecimals' => env('DEFAULT_DECIMALS', 2),
	'defaultDecimalPoint' => env('DEFAULT_DECIMAL_POINT', '.'),
	'defaultThousandSeparator' => env('DEFAULT_THOUSAND_SEPERATOR', ','),

	'messageClass' => env('MESSAGE_CLASS', "{'col-sm-offset-3': true, 'col-sm-9': true}"),

	'debounceDelay' => env('DEBOUNCE_DELAY', "500"),
];
